<?php
/**
 * Class BaseModel
 *
 * Tout élément enfant de BaseModel pourra être utilisé pour interroger la base facilement.
 * Chaque enfant doit définir le nom de la table dans le champ $tableName de manière statique (protected static $tableName = "nomtable")
 * Un enfant pourra sélectionner des données de manière dynamique avec les méthode select, update, insert et delete.
 * La méthode "insertObject" peut être utilisée sur les instances de classe ($user->insert()), la requête est générée automatiquement).
 * Les autres méthodes sont uniquement utilisables de manière statique (exemples) :
 * - User::select($where: assoc array)
 *      Selectionne une liste d'objet
 *      $where comprend un tableau associatif: ["name" => "Jean DUPOND"]
 *             > cela signifie que la recherche sera effectuée sur le champ name et résultera toutes les lignes dont name équivaut à "Jean DUPOND"
 *          Le critère est par défaut utilisée pour tester une égalité. Si la condition doit être différente, voici comment procéder
 *          ["age" => [">", 15]]
 *             > cela signifie que la recherche sera effectuée sur le champ age et résultera toutes les lignes dont age est strictement supérieur à 15
 *
 * - User::execute($customQuery, $where: assoc array)
 *      Execute une requête et retourne un tableau d'objets
 *      L'argument where comprend un tableau associatif étant indexé par le nom du bind (:bind) sans le caractères ":" et portant comme valeur la valeur à binder.
 * - User::executeOne($customQuery, $where: assoc array)
 *      Execute une requête et retourne un objet
 * - User::updateObject($value: assoc array, $where: assoc array)
 *      Met à jour les données de la table User renseignées dans le tableau $value à condition de remplir les paramètres $where
 * - User::update($table, $value: assoc array, $where: assoc array)
 *      Met à jour les données de la table $table renseignées dans le tableau $value à condition de remplir les paramètres $where
 * - User::insertObject(?$values: assoc array)
 *      Insert une ligne en utilisant ou non les champs de l'object (si $values renseigné, les données insérées seront celles renseignées dans $values)
 */
use Lazer\Classes\Database as Lazer;


abstract class BaseModel
{
    private static $_lazerInstance = null;

    public static final function getTableName(): string
    {
        return static::$tableName;
    }

    public static final function getCollumn($name): string
    {
        return static::$tableName . '.' . $name;
    }

    /**
     * @return \Lazer\Classes\Database
     */
    public static final function lazer()
    {
        self::checkDB();
        try {
            self::$_lazerInstance = Lazer::table(self::getTableName());
        } catch (Exception $e) {
            echo "error" . $e->getMessage();
        }
        return self::$_lazerInstance;
    }

    public static function parseJsonOrText($string)
    {
        $r = json_decode($string);
        if ($r === null)
            return $string;
        return $r;
    }

    private static function checkDB()
    {
        try {
            \Lazer\Classes\Helpers\Validate::table(self::getTableName())->exists();
        } catch (\Lazer\Classes\LazerException $e) {
            get_called_class()::createTable();
        }
    }

    /**
     * @param $listOrElement Lazer
     * @return BaseModel[]
     */
    protected static function map($listOrElement) {
        $r = [];
        if($listOrElement->count() > 0) {
            foreach ($listOrElement->asArray() as $row) {
                /** @var $listOrElement Lazer */
                $r[] = Utils::cast((object) $row, get_called_class());
            }
        }
        return $r;
    }

    /**
     * @return array string (associative)
     */
    protected function toArray(): array
    {
        $reflection = new ReflectionObject($this);

        $fp = array_filter($reflection->getProperties(ReflectionProperty::IS_PRIVATE), function (ReflectionProperty $o) {
            return $o->getName()[0] !== '_';
        });
        $r = [];
        foreach ($fp AS $v) {
            /** @var ReflectionProperty $v */
            $v->setAccessible(true);
            if ($v->getValue($this) !== NULL)
                $r[$v->getName()] = $v->getValue($this);
            $v->setAccessible(false);
        }
        return $r;
    }

    /**
     * @return array string
     */
    protected static function getClassFields(): array
    {
        $reflection = new ReflectionClass(get_called_class());

        $fp = array_filter($reflection->getProperties(ReflectionProperty::IS_PRIVATE), function (ReflectionProperty $o) {
            return $o->getName()[0] !== '_';
        });
        $r = [];
        foreach ($fp AS $rp) {
            /** @var ReflectionProperty $rp */
            $r[] = self::getTableName() . '.' . $rp->getName();
        }
        return $r;
    }

    abstract static function createTable();
}