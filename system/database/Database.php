<?php

/**
 * Classe Base de donnée
 */
class Database
{
    /**
     * TODO Comment
     * @return \Pixie\Connection
     */
    public static function getQueryBuilder()
    {
        $config = array(
            'driver' => Configuration::get('db_type'),
            'host' => Configuration::get('db_host'),
            'database' => Configuration::get('db_name'),
            'username' => Configuration::get('db_user'),
            'password' => Configuration::get('db_pwd'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => Configuration::get('db_prefix'),
        );

        return new \Pixie\Connection(Configuration::get('db_type'), $config, Configuration::get('qb_alias'));
    }


    /**
     * TODO Comment
     * @param null $dbName
     * @param null $dbHost
     * @param null $dbUser
     * @param null $dbPwd
     * @return PDO
     */
    public static function getMysqlDatabase($dbName = null, $dbHost = null, $dbUser = null, $dbPwd = null)
    {
        $dbName = $dbName !== null ? $dbName : Configuration::get('db_name');
        $dbHost = $dbHost !== null ? $dbHost : Configuration::get('db_host');
        $dbUser = $dbUser !== null ? $dbUser : Configuration::get('db_user');
        $dbPwd = $dbPwd !== null ? $dbPwd : Configuration::get('db_pwd');

        return new PDO("mysql:dbname=" . $dbName . ";host=" . $dbHost . "", $dbUser, $dbPwd,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    }

    /**
     * TODO Comment
     * @return PDO
     */
    public static function getSQLiteDatabase()
    {
        $pdo = new PDO("sqlite::memory:", '', '');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        return $pdo;
    }
}