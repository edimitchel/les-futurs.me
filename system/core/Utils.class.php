<?php

class Utils
{
    public static function genererClasseDepuisBDD($dir = "", $classExt = "", $supprimerExistantes = false)
    {
        $db = Database::getMysqlDatabase();

        $tables = $db->query('show tables;')->fetchAll();

        $dir = $dir !== '' ? $dir : APP_PATH . 'models/';
        $classExt = $classExt !== '' ? $classExt : '.class.php';

        $template = '<?php

/**
 * %s
 */
class %s extends BaseModel
{
    protected static $tableName = "%s";
    
    private %s;

}';

        if (!file_exists($dir)) {
            mkdir($dir);
        } elseif($supprimerExistantes) {
            self::deleteFiles($dir);
        }

        foreach ($tables as $table) {
            $tableName = $table["Tables_in_aacv2"];
            $className = implode("", array_map(function ($n) {
                return ucfirst($n);
            }, explode("_", $table["Tables_in_aacv2"])));

            $q = $db->query('DESCRIBE ' . $tableName);
            $fs = $q->fetchAll();
            $fields = implode(', ', array_map(function ($c) {
                return '$' . $c[0];
            }, $fs));


            $class = sprintf($template, $className, $className, $tableName, $fields);

            $filePath = $dir . $className . $classExt;

            $file = fopen($filePath, 'w+');
            fwrite($file, $class);
            chmod($dir . $className . $classExt, 0660);
        }
    }

    public static function deleteFiles($dir, $deleteIhmSelf = false)
    {
        if (!$dir) return;

        $files = scandir($dir);

        foreach ($files as $file) {
            if ($file == '.' || $file == '..')
                continue;

            $filePath = $dir . $file;
            if (is_dir($filePath)) {
                if (is_dir($filePath)) {
                    $objects = scandir($filePath);
                    foreach ($objects as $object) {
                        if ($object != "." && $object != "..") {
                            if (is_dir($filePath . "/" . $object))
                                rmdir($filePath . "/" . $object);
                            else
                                unlink($filePath . "/" . $object);
                        }
                    }
                    rmdir($filePath);
                }
            } else unlink($filePath);
        }

        if ($deleteIhmSelf)
            rmdir($dir);
    }

    public static function cast($obj, $to_class) {
        if(class_exists($to_class)) {
            $obj_in = serialize($obj);
            $obj_out = 'O:' . strlen($to_class) . ':"' . $to_class . '":' . substr($obj_in, $obj_in[2] + 7);
            return unserialize($obj_out);
        }
        else
            return false;
    }
}