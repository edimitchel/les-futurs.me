<?php

define('IMAGE_FOLDER', Paths::ASSETS . "images" . "/");
define('CSS_FOLDER', Paths::ASSETS . "css" . "/");
define('JS_FOLDER', Paths::ASSETS . "js" . "/");

class FrontController
{
    const IMAGE_FOLDER = IMAGE_FOLDER;

    const CSS_FOLDER = CSS_FOLDER;

	const JS_FOLDER = JS_FOLDER;

	public static final function dispatch(){
		session_start();

		$request = Request::getByRoute();
		$response = Response::get();

		$view = ActionController::process($request, $response);

		$view->display();
	}

	/**
	 * @param $pageName
	 */
	public static final function setNotFoundPage($pageName){
		Request::setNotFoundPage($pageName);
	}

	/**
	 * @param bool $url
	 */
	public static final function redirect($url){
		if($url){
			header("Location: " . $url);
			die;
		}
	}

	/**
	 * @param $name
	 * @return string
	 */
	public static final function getImageUri($name){
		return $pathFile = "/" . self::IMAGE_FOLDER . $name;
	}

	/**
	 * @param $name
	 * @param bool $holdExt
	 * @return string
	 */
	public static final function getStyleUri($name, $holdExt = false){
		// On supprime l'extension du fichier.
		$parts = explode(".", $name);
		if(sizeof($parts > 1) && !$holdExt){
			$name = "";
			for ($i=0; $i < sizeof($parts); $i++) {
				$name .= $parts[$i];
				if($i < sizeof($parts) - 1)
					$name .= ".";
			}
		}

		return $pathFile = "/" . self::CSS_FOLDER . $name . ($holdExt ? "" : ".css");
	}

	/**
	 * @param $name
	 * @param bool $holdExt
	 * @return string
	 */
	public static final function getStyleUriFromVendor($name, $holdExt = false){
		// On supprime l'extension du fichier.
		$parts = explode(".", $name);
		if(sizeof($parts > 1) && !$holdExt){
			$name = "";
			for ($i=0; $i < sizeof($parts); $i++) { 
				$name .= $parts[$i];
				if($i < sizeof($parts) - 1)
					$name .= ".";
			}
		}

		return $pathFile = "/" . Paths::VENDOR . $name . ($holdExt ? "" : ".css");
	}

	/**
	 * @param $name
	 * @param bool $holdExt
	 * @return string
	 */
	public static final function getJsUri($name, $holdExt = false){
		// On supprime l'extension du fichier.
		$parts = explode(".", $name);
		if(sizeof($parts > 1) && !$holdExt){
			$name = "";
			for ($i=0; $i < sizeof($parts); $i++) { 
				$name .= $parts[$i];
				if($i < sizeof($parts) - 1)
					$name .= ".";
			}
		}

		return $pathFile = "/" . self::JS_FOLDER . $name . ($holdExt ? "" : ".js");
	}

	/**
	 * @param $name
	 * @param bool $holdExt
	 * @return string
	 */
	public static final function getJsUriFromVendor($name, $holdExt = false){
		// On supprime l'extension du fichier.
		$parts = explode(".", $name);
		if(sizeof($parts > 1) && !$holdExt){
			$name = "";
			for ($i=0; $i < sizeof($parts); $i++) { 
				$name .= $parts[$i];
				if($i < sizeof($parts) - 1)
					$name .= ".";
			}
		}

		return $pathFile = "/" . Paths::VENDOR . $name . ($holdExt ? "" : ".js");
	}

	const PREFIX_SESSION = "app_a_code_";

	/**
	 * @param $key
	 * @param bool $default
	 * @return mixed|$default
	 */
	public static final function getSessionData($key, $default = false){
		if(isset($_SESSION[self::PREFIX_SESSION . $key])){
			return $_SESSION[self::PREFIX_SESSION . $key];
		}
		return $default;
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public static final function setSessionData($key, $value){
		$_SESSION[self::PREFIX_SESSION . $key] = $value;
	}

	/**
	 * @param $key
	 */
	public static function removeSessionData($key)
	{
		if(isset($_SESSION[self::PREFIX_SESSION . $key]))
			unset($_SESSION[self::PREFIX_SESSION . $key]);
	}

	const PREFIX_COOKIE = "aac_";

	/**
	 * @param $key
	 * @param bool $default
	 * @return string
	 */
	public static final function getCookieData($key, $default = false){
		if(isset($_COOKIE[self::PREFIX_COOKIE . $key])){
			return $_COOKIE[self::PREFIX_COOKIE . $key];
		}
		return $default;
	}

	/**
	 * @param $key
	 * @param $value
	 * @param int $timeValidity
	 * @param string $path
	 * @param bool $secure
	 * @return bool
	 */
	public static final function setCookie($key, $value, $timeValidity = 0, $path = "/", $secure = true){
		return setcookie($key, $value, $timeValidity, $path, null, $secure);
	}
}