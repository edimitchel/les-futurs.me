<?php

/**
 *
 */
class ActionController
{

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @param Request $request
     * @param Response $response
     */
    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    /**
     * Récupère le nom de la classe du contrôleur à charger.
     * @param $module
     * @return string
     */
    public static function getClass($module)
    {
        $class = ucfirst($module) . "Controller";

        return $class;
    }

    /**
     * Récupère le chemin vers le contrôleur à charger. Renvoit false si le contrôleur et la classe est introuvable.
     * @param $module
     * @return bool|string
     */
    public static function controllerExists($module)
    {
        $ext = ".class.php";
        $path = APP_PATH . "controllers/";
        $class = self::getClass($module);
        $file = $path . $class . $ext;

        if (!file_exists($file) && !class_exists($class)) {
            // Si le contrôleur n'existe pas.
            return false;
        }
        return $file;
    }

    /**
     * Permet de récupérer le bon contrôleur le module renseigné (dans l'url)
     * Puis execute (par la méthode launch) le contrôleur qui va enclencher la bonne action qui est récupérée dans l'url.
     * @param Request $request
     * @param Response $response
     * @return BaseController
     */
    public static final function process(Request $request, Response $response)
    {
        $file = self::controllerExists($request->getModule());

        if ($file === false) {
            // Si le contrôleur n'existe pas, on affiche le contrôleur permettant d'afficher une page 404.
            $request->setNoFoundRequest($response);
            $file = self::controllerExists($request->getModule());
        }

        $class = self::getClass($request->getModule());

        /** @noinspection PhpIncludeInspection */
        require_once $file;

        /** @var BaseController $controller */
        $controller = new $class($request, $response);

        $controller->launch();

        return $controller;
    }

    /**
     * @param Task $t
     */
    public static final function addTask(Task $t)
    {
        array_push(BaseController::$tasks, $t);
    }

    /**
     * @param bool $i
     * @return array|bool
     */
    public final function getDatas($i = false)
    {
        return $this->request->getData($i);
    }

    /**
     * @return Response
     */
    protected final function getResponse()
    {
        return $this->response;
    }
}