<?php

if (!function_exists('getRouteUrl')) {
    function getRouteUrl($module, $action = "", ...$data)
    {
        $config = Configuration::get('routeRender');
        $route = '/';

        if($config === 'query') {
            $route .= sprintf('?module=%s', $module);
            if($action)
                $route .= sprintf('&action=%s', $action);
            if(sizeof($data) > 0)
                $route .= sprintf('&data=%s', implode('/', $data));
        } else {
            $route .= sprintf('%s', $module);
            if($action)
                $route .= sprintf('/%s', $action);
            if(sizeof($data) > 0)
                $route .= sprintf('/%s', implode('/', $data));
        }

        return $route;
    }
}