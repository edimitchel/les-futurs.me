<?php

class Configuration
{
    private static $_instance = null;

    private $config_list = array();

    private $isSaved = false;

    private function __construct()
    {
    }

    public static function get($name = null)
    {
        if ($name === null) {
            if (self::$_instance === null)
                self::$_instance = new self;
            return self::$_instance;
        } else {
            if ($name == "*")
                return self::get()->config_list;
            else if (isset(self::get()->config_list[strtoupper($name)]))
                return self::get()->config_list[strtoupper($name)]->value;
            return null;
        }
    }

    public static function add($name, $value, $modifier = "")
    {
        if (self::get()->get($name) !== null)
            return;

        $config = array(
            "name" => self::getName($name),
            "value" => self::getValue($value),
            "modifier" => $modifier,
            "isSaved" => false
        );

        $list = 'config_list';

        self::get()->{$list}[self::getName($name)] = (object)$config;

        self::get()->onChange();
    }

    private static function getName($name, $upper = true)
    {
        if ($upper === true)
            return strtoupper(trim($name));
        else
            return trim($name);
    }

    private static function getValue($value)
    {
        if (trim($value) === "true" || trim($value) === "1") {
            return true;
        } else if (trim($value) === "false" || trim($value) === "0") {
            return false;
        }
        return trim($value);
    }

    /*
    * Provide configuration from a config file.
    */
    public static function getConfigFromFile($filename)
    {
        if (($r = self::get('root')) != null)
            $filename = $r . '/system/' . $filename;

        if (!file_exists($filename)) {
            die("The config file '" . $filename . "' doesn't exist.");
        }

        $conf = file_get_contents($filename);

        preg_match_all('#(\+?)(\w+) ?= ?(.*)#', $conf, $matches);
        if (($l = sizeof($matches)) > 1) {
            for ($i = 0; $i < sizeof($matches[1]); $i++) {
                if (!(trim($matches[2][$i])[0] == '#')) {
                    self::get()->add($matches[2][$i], $matches[3][$i], $matches[1][$i]);
                }
            }
        }
    }

    private function onChange()
    {
        foreach ($this->config_list AS $c) {
            if (!defined($c->name) && $c->modifier == "+" && !$c->isSaved) {
                define($c->name, $c->value);
                $c->isSaved = true;
            }
            if ($c->modifier == "s") {
                AACSmarty::getInstance()->assign(strtolower($c->name), $c->value);
                $c->isSaved = true;
            }
        }
    }
}