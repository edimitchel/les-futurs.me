<?php

/**
 * Class BaseController
 *
 * Le routage fonctionne de la manière suivante :
 *  Voici la forme d'une url : /:module/:action/:data
 * - le module pointe sur le contrôle (nommodule avec la première lettre en majuscule : NomModuleController)
 * - l'action point sur une méthode du contrôleur (nomAction pointe sur la méthode _nomAction du contrôleur NomModuleController, si nomAction n'est pas renseigné, c'est la méthode index qui est utilisée)
 * - les data peuvent être transmises par l'url sous la forme de .../data1/data2/data3 ou par l'intermédiaire des données GET/POST
 *
 *
 * Les contrôleurs sont composés de trois types de méthodes :
 * - la méthode index qui est exécutée lorsqu'aucune action n'est renseignée
 * - les méthodes de vues identifiées par le prefix "_"
 * - les méthodes AJAX identifiées par le prefix "a_"
 */
abstract class BaseController extends ActionController
{
    /**
     * @var Smarty
     */
    protected $smarty;

    protected $isDataController = false;

    protected $isIndex = false;

    private $isDisplay = false;

    const CONTROLLER_PREFIX = "_";

    const AJAX_METHOD_PREFIX = "a_";

    protected static $tasks = array();

    protected function before()
    {
        $_SESSION['org_referer'] = $_SERVER['HTTP_REFERER'] ?? null;
        $this->request->setOriginUrl($_SESSION['org_referer']);
    }

    protected function after()
    {
    }

    /**
     * Action par défaut d'un contrôleur
     * @return mixed
     */
    public abstract function index();

    /**
     *
     */
    public final function launch()
    {
        $this->smarty = AACSmarty::getInstance();
        $this->before();

        $this->smarty->assign('pageId', $this->request->getModule());

        $this->runTasks();

        $action = $this->request->getAction();
        if ($action !== NULL) {
            if (method_exists($this, $this->getPrefixMethod() . $action)) {
                $this->$action();
            } else {
                $this->isIndex = true;
                $this->request->setNoFoundRequest($this->response);
            }
        } else {
            $this->isIndex = true;
            $this->index();
        }
    }

    private final function getPrefixMethod(): string
    {
        $isAjax = $this->request->isAjaxRequest();
        return ($isAjax && !$this->isDataController) ? self::AJAX_METHOD_PREFIX : self::CONTROLLER_PREFIX;
    }

    /**
     * Execute les tâches
     */
    private final function runTasks()
    {
        foreach (self::$tasks as $t) {
            $t->run($this);
        }
    }

    /**
     * @param $method
     * @param $arguments
     * @return void
     */
    public final function __call($method, $arguments)
    {
        if ($method != "index" && $method != "display" && $method != "launch" && $method != "configure") {
            $a = $this->getPrefixMethod() . $method;
            call_user_func_array(array($this, $a), $this->getDatas());
        }
    }

    /**
     * @param $key
     * @param $value
     */
    public final function attach($key, $value)
    {
        $this->smarty->assign($key, $value);
    }

    /**
     * @param boolean $isDataController
     */
    public function setIsDataController($isDataController)
    {
        $this->isDataController = $isDataController;
    }

    /**
     * Affiche le template
     * @param bool|string $page string
     * @throws Exception
     */
    public final function display($page = false)
    {
        if (!$this->isDisplay) {
            if (!$this->request->isAjaxRequest()) {
                if ($page === false) {
                    if (!$this->isIndex && $this->smarty->templateExists($this->request->getModule() . '_' . $this->request->getAction() . '.tpl')) {
                        $this->smarty->display($this->request->getModule() . '_' . $this->request->getAction() . '.tpl');
                    } else if ($this->isIndex) {
                        $this->smarty->display($this->request->getModule() . '.tpl');
                    } else {
                        throw new Exception("Template " . $this->request->getModule() . '_' . $this->request->getAction() . '.tpl' . ' introuvable');
                    }
                } else {
                    $this->smarty->display($page . ".tpl");
                }
                $this->isDisplay = true;
            } else {
                header("Content-typ: application/json");
                die($this->response->toString());

            }
            $this->after();
        }
    }
}