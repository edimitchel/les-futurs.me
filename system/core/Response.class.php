<?php

/**
* 
*/
class Response
{
	private $status = true;

	private $data = array();

	private $error = array();

	/**
	 * @return Response
	 */
	public static function get()
	{
		$r = new Response();
		return $r;
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public final function setData($key, $value){
		$this->data[$key] = $value;
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public final function setError($key, $value){
		$this->error[$key] = $value;
		$this->setStatus(false);
	}

	/**
	 * @param boolean $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

    /**
     * @param string $format
     * @return string
     * @throws Exception
     */
	public final function toString($format = "json"){
		if($format == "json"){
			$r = array("status" => $this->status);

			if($this->status)
				$r["data"] = $this->data;
			else
				$r["error"] = $this->error;

			return json_encode($r);
		} else {
		    throw new Exception("No format display defined for ajax response");
        }
	}
}