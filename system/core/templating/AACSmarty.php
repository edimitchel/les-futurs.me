<?php

	class AACSmarty
	{

		private static $_instance = null;

		private function __construct() {}
		
		public static function getInstance($newInstance = false) {
			if(self::$_instance === null || $newInstance) {
                include_once "helpers.smarty.php";

				$smarty = new Smarty();
				$smarty->setTemplateDir(Paths::TEMPLATE);
				$smarty->setCompileDir(Paths::TPL_COMPILE);
				$smarty->setConfigDir(Paths::TPL_CONFIG);
				$smarty->setCacheDir(Paths::TPL_CACHE);

                if(Configuration::get('env') === "development") {
                    $smarty->caching = false;
                    $smarty->force_compile = true;
                }

                // Définition des variables globales
                $smarty->assign("app_name", "Les futurs M&Mme Edighoffer - MMM");
                $smarty->assign("environment", Configuration::get('env'));
                $smarty->assign("routes", getRoutes());

                if($newInstance)
                    return $smarty;

				self::$_instance = $smarty;
			}
			return self::$_instance;
		}				
	}