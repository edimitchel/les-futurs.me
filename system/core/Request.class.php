<?php




/**
 *
 */
class Request
{
    const INT = 'integer';
    const FLOAT = 'double';
    const EMAIL = 'email';

    const INDEX_REQUEST = "home";

    private static $NOTFOUND_REQUEST = "notfound";

    private $module;
    private $action;
    private $data = array();

    private $originUrl;

    private $ajaxRequest = false;

    private function determinateAjaxRequest()
    {
        $this->ajaxRequest = (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    /**
     * @return boolean
     */
    public function isAjaxRequest(): bool
    {
        return $this->ajaxRequest;
    }

    /**
     *
     * @return Request
     */
    public static function getByRoute()
    {
        $r = new Request();
        $r->determinateAjaxRequest();
        $r->route();
        return $r;
    }

    /**
     * @param $pagename
     * @throws Exception
     */
    public static function setNotFoundPage($pagename)
    {
        if(false === ActionController::controllerExists($pagename)) {
            throw new Exception("Le contrôleur 'Page non trouvée' est inaccessible");
        } else {
            self::$NOTFOUND_REQUEST = $pagename;
        }
    }

    function setIndexRequest()
    {
        $this->module = self::INDEX_REQUEST;
    }

    function setOriginUrl($url) {
        $this->originUrl = $url;
    }

    function getOriginUrl() {
        return $this->originUrl ?? '';
    }

    function setNoFoundRequest(&$response)
    {
        $r = Response::get();
        $r->setError("routing", "No route found : " . $this->getModule() . ($this->getAction() ? '.' . $this->getAction() : ''));
        $response = $r;

        $this->module = self::$NOTFOUND_REQUEST;
        $this->action = null;
    }

    public function route()
    {
        $this->module = self::get('module');
        if ($this->module == false || empty($this->module))
            $this->setIndexRequest();
        $this->action = self::get('action');

        $data = self::get('data');
        $this->data = explode("/", $data);
    }

    public function getModule($escape = true)
    {
        if(isset($this->module) && $escape)
            return str_replace('-', '', $this->module);

        return $this->module;
    }


    public function getAction($escape = true)
    {
        if(isset($this->action) && $escape)
            return str_replace('-', '', $this->action);

        return $this->action;
    }

    /**
     * @param bool $i Index de la donnée à récupérer
     * @return array|bool
     */
    public function getData($i = false)
    {
        if ($i !== false && ($i < 0 || $i > sizeof($this->data) - 1))
            return false;
        return $i === false ? $this->data : $this->data[$i];
    }

    /**
     * @param $key
     * @param $type string INT, FLOAT, EMAIL. String est par défaut
     * @return mixed La donnée si le type est bonne, sinon FALSE.
     * Si la donnée n'existe, ça retourne NULL.
     */
    public final static function get($key, $type = null)
    {
        return isset($_GET[$key]) ? self::checkType($_GET[$key], $type) : null;
    }

    /**
     * @param $key
     * @param $type string INT, FLOAT, EMAIL. String est par défaut
     * @return mixed La donnée si le type est bonne, sinon FALSE.
     * Si la donnée n'existe, ça retourne NULL.
     */
    public final static function post($key, $type = null)
    {
        return isset($_POST[$key]) ? self::checkType($_POST[$key], $type) : null;
    }

    /**
     * @param $val
     * @param $type
     * @return mixed La donnée si le type est bon, sinon FALSE
     */
    private final static function checkType($val, $type) {
        if($type !== null){
            $TYPE = null;
            switch ($type) {
                case 'int':
                case 'integer':
                    $TYPE = FILTER_VALIDATE_INT;
                    break;
                case 'double':
                case 'float':
                    $TYPE = FILTER_VALIDATE_FLOAT;
                  break;
                case 'email':
                case 'mail':
                    $TYPE = FILTER_VALIDATE_EMAIL;
                break;
                default:
                    $TYPE = FILTER_SANITIZE_STRING;
                    break;
            }
            return filter_var($val, $TYPE);
        }
        return $val;
    }
}