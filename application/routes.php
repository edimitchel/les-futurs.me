<?php

function getRoutes(){
    $routes = [
    ];

    array_walk($routes, function(&$r, $kr){
        $r = $kr . " : '" . $r . "'";
    });

    return "{" .
        implode(",", $routes)
    . "}";
}