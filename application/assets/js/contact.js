(function ($) {

    var contactForm = $('#formContact');

    contactForm.submit(function (evt) {

        var type = $(this).find('#cnttypeMsg'),
            nom = $(this).find('#cntnom'),
            adresse = $(this).find('#cntemail'),
            message = $(this).find('#cntmessage');
        $.ajax({
            url: '/home/contact/' + type.val(),
            type: 'POST',
            dataType: 'json',
            data: {
                nom: nom.val(),
                adresse: adresse.val(),
                message: message.val()
            }
        }).done(function (res) {
            if (res.status) {
                // OK
                contactForm.before('<h4>' + res.data.message + '</h4>');
                contactForm[0].reset();
                contactForm.slideUp(400);

            } else {
                if(res.error.data) alert(res.error.data);
                if(res.error.mail) alert(res.error.mail);
            }
        });

        evt.preventDefault();
        return false;
    })

})(jQuery);