(function ($, ctx) {

    var confType = {
            ceremonie: "la cérémonie",
            soiree: "toute la célébration (cérémonie + soirée)"
        },
        confirmForm = $('#confirmForm'),
        typeSelected;

    function confirmation(type) {
        typeSelected = type;
        var title = confType[type];

        if (title) {
            confirmForm.find('h4 span').text(title);
            confirmForm.removeClass('hidden');

            $('body').delay(150).animate({
                scrollTop: confirmForm.offset().top - 15
            }, 300);
        }

        confirmForm.next('.msg').remove();

        $(this).addClass('alt2');
    }

    confirmForm.submit(function (evt) {

        var nom = $(this).find('#cnfnom'),
            nbpersonne = $(this).find('#cnfnbpersonne'),
            adresse = $(this).find('#cnfemail'),
            phone = $(this).find('#cnfphone');

        $.ajax({
            url: '/home/confirm/',
            type: 'POST',
            dataType: 'json',
            data: {
                type: typeSelected,
                nom: nom.val(),
                nbpersonne: nbpersonne.val(),
                adresse: adresse.val(),
                phone: phone.val()
            }
        }).done(function (res) {
            if (res.status) {
                // OK
                // TODO Confirmer l'envoi du mail.
                confirmForm[0].reset();
                confirmForm.after('<strong class="msg">' + res.data.message + '</strong>');
                confirmForm.addClass('hidden');
            } else {
                if (res.error.data) alert(res.error.data);
            }
        });


        evt.preventDefault();
        return false;
    }).find('input[type="reset"]').click(function () {
        confirmForm[0].reset();
        confirmForm.addClass('hidden');
    });

    ctx.confirmPresence = confirmation;

})(jQuery, window);