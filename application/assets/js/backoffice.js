(function ($) {

    $('a.delete-conf').click(function (evt) {

        var that = $(this),
            id = parseInt($(this).attr('data-id'));

        if (!isNaN(id) && confirm('Sûr de vouloir supprimer cette confirmation ?')) {
            $.ajax({
                url: '/backoffice/deleteconf/' + id + '?token=' + gup('token', location),
                type: 'GET',
                dataType: 'json'
            }).done(function (res) {
                if (res.status) {
                    alert(res.data.message);
                    that.parents('tr').remove();
                    updateSum();
                } else {
                    alert(res.error.message);
                }
            });
        }

        evt.preventDefault();
        return false;
    });

    var total = $('#totalPersonne'),
        body;

    function updateSum() {
        body = total.parents('table').find('tbody tr')
        var t = 0;
        $.each(body, function(e){
            t += parseInt($(this).find('td:eq(2)').text());
        });
        total.text(isNaN(t) ? '' : t);
    }
    updateSum();

    function gup( name, url ) {
        if (!url) url = location.href;
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( url );
        return results == null ? null : results[1];
    }

})(jQuery);