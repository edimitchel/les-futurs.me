<?php

use Lazer\Classes\Database as Lazer;

class Confirmation extends BaseModel
{

    static $tableName = 'confirmation';

    public $id, $type, $nom, $nb_personne, $email, $telephone;


    public function create() {
        $l = self::lazer();
        $l->type = $this->getType();
        $l->nom = $this->getNom();
        $l->nb_personne = $this->getNbPersonne();
        $l->email = $this->getEmail();
        $l->telephone = $this->getTelephone();

        $l->save();

        return $this;
    }

    public static function getAll() {
        $r = self::lazer()
            ->orderBy('nom')->orderBy('type')
            ->findAll();

        return self::map($r);
    }

    /**
     * @param $id
     * @return bool
     */
    public static function delete($id) {
        $r = self::lazer()
            ->where('id', '=', intval($id))
            ->delete();

        return $r;
    }

    static function createTable()
    {
        Lazer::create(self::getTableName(), array(
            'id' => 'integer',
            'type' => 'string',
            'nom' => 'string',
            'nb_personne' => 'integer',
            'email' => 'string',
            'telephone' => 'string'
        ));
        \Lazer\Classes\Relation::table(self::getTableName())->localKey('id');
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Confirmation
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param $formatted
     * @return mixed
     */
    public function getType($formatted = false)
    {
        if($formatted){
            return $this->type === 'ceremonie' ? 'Cérémonie' : 'Soirée';
        }

        return $this->type;
    }

    /**
     * @param mixed $type
     * @return Confirmation
     */
    public function setType($type)
    {
        if(!in_array($type, ['ceremonie', 'soiree']))
            $type = 'ceremonie';

        $this->type = strtolower($type);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     * @return Confirmation
     */
    public function setNom($nom)
    {
        $this->nom = strtoupper(strip_tags($nom));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNbPersonne()
    {
        return $this->nb_personne;
    }

    /**
     * @param mixed $nb_personne
     * @return Confirmation
     */
    public function setNbPersonne($nb_personne)
    {
        $this->nb_personne = $nb_personne;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return Confirmation
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     * @return Confirmation
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
        return $this;
    }



}