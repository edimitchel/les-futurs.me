{extends file="struct/template.tpl"}
{block name="title"}Qui sont-il donc ?{/block}
{block name="header"}
    <button class="button alt2"><i class="fa fa-home"></i> Accueil</button>
{/block}
{block name="content"}
    <div class="box" id="presentationmaries">

        <h1 class="big">Qui sont ces futurs mariés ?</h1>

        <img class="image" src="{imguri name="les-futurs-maries.jpg"}" alt="Les futurs mariés">
        <br>
        <div class="row">
            <div class="6u 12u(mobilep) left">
                <h2>Mathilde CAREL – {$ageMathilde} ans</h2>
                <div class="content">
                    <p class="normal">
                        <img src="{imguri name="mathilde/bebe.png"}" alt="Bébé" class="image-rounded" width="200" align="left">Moi c’est Mathilde et je suis née le 29 mai 1992 à Saverne.<br><br><img src="{imguri name="mathilde/pre-ado.png"}" alt="Avec ma soeur" class="image-rounded" width="150" align="right">
                        J’ai toujours aimé passer du temps à dessiner et “regarder comment les choses sont faites” comme dirait ma mamie. D’ailleurs ça m’a bien servi puisque maintenant j’utilise mes mains pour faire du graphisme, de la broderie, et de temps en temps de la cuisine (manger?).<br><br>
                        <img src="{imguri name="mathilde/beaute.png"}" alt="En mode beauté" class="image-rounded" width="150" align="left"><img src="{imguri name="mathilde/adulte.png"}" alt="À la plage" class="image-rounded" width="200" align="right">Plein de petites choses que j’ai hâte de faire grandir entre 4 futurs murs. Que cela soit un petit coin de bonheur pour les personnes qui y vivront et celles qui y passeront.<br><br>
                        Mais pour construire cet endroit et qu’il puisse être un témoignage, j’aurai besoin de la personne que j’attendais depuis toute petite.<br><br>
                    </p>
                    <p>Depuis 4 ans et demi nous construisons des fondations, et depuis deux ans environs nous avons choisi de les solidifier et d’y faire vivre notre engagement.</p>
                </div>
            </div>
            <div class="6u 12u(mobilep) right">
                <h2>Michel EDIGHOFFER – {$ageMichel} ans</h2>
                <div class="content">
                    <p class="normal"><img src="{imguri name="michel/bebe.jpeg"}" alt="Bébé" class="image-rounded" width="200" align="left">Moi c’est Michel et je suis né 9 mois plus tard le 28 février 1993 à Wissembourg – entouré par mes frères et quelques années plus tard, ma sœur.<br><br><img src="{imguri name="michel/frere-soeur.jpeg"}" alt="Mes frères et sœur" class="image-rounded" width="150" align="right">Du haut de ma petite moustache et ma petite barbe, j’avance dans plusieurs projets de vie dont notre mariage. J’aime passer des petits temps simples avec ma fiancée et partager nos soucis, prier ensemble, et rigoler mais aussi d’être sérieux.<br><br>
                        <img src="{imguri name="michel/velo.jpg"}" alt="Vélo" class="image-rounded" width="150" align="left">Au-delà de ce grand projet, je me passionne pour l’aéromodélisme et l’informatique. Ces dernières années il y a la possibilité de faire cohabiter les deux avec l’arrivée des drones – pour lesquels je m’intéresse depuis début 2016.<br><br>
                        <img src="{imguri name="michel/pre-ado.jpg"}" alt="En mode beau gosse" class="image-rounded" width="200" align="right">Et au-delà de mes passions, je me considère comme un petit aventurier qui aime prendre des risques et découvrir des choses techniquement intéressantes (j’aime la mécanique).<br><br>
                        Ça fait 2 ans que nous préparons ensemble notre union et j’ai souvent eu du mal à m’imaginer certains aspects d’organisation (étant moi-même pas très organisé).
                        On a avancé malgré les doutes et les conflits que l’on a pu rencontrer.<br><br>
                    </p>
                    <p>J’espère que ce mariage, sera avant tout un témoignage de notre engagement pour les personnes qui n’ont plus confiance en l’engagement conjugal.</p>
                </div>
            </div>
        </div>
    </div>
{/block}
{block name="moreScript"}
{/block}
