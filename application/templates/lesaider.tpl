{extends file="struct/template.tpl"}
{block name="title"}Les aider à construire la suite{/block}
{block name="header"}
	<button class="button alt2"><i class="fa fa-home"></i>Accueil</button>
{/block}
{block name="content"}
    <header class="major container">
        <p>Nous avons décidé d'emménager après le mariage et cela demande une certaine organisation en plus.
            <br>Vous pouvez nous souvenir en vous engageant à contribuer à la liste de mariage ci-après :</p>
        <p><strong>NON-DISPONIBLE POUR LE MOMENT – liste en construction</strong></p>
        <ul class="vertical actions">
            <li><a href="http://mmm-1-7-17.ameliste.fr" target="_blank" class="button">Accéder à la liste de mariage</a></li>
        </ul>

        <a href="http://mmm-1-7-17.ameliste.fr" target="_blank"><img class="image" src="{imguri name="ameliste.png"}" alt=""></a>
    </header>

{/block}
{block name="moreScript"}
{/block}
