<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
    <h1>Les futurs mariés</h1>
    <p>Un message vous a été transmis :</p>
    <br>
    <p style="font-weight: bold; padding: 15px; border: 1px dotted #913131; font-size: 1.2em;">
        {$message}
    </p>
</body>
</html>