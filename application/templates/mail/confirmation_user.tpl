<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
    <h1>Confirmation de présence</h1>
    <p>Merci {$nom}, d'avoir confirmer votre présence.
        <br>Nous sommes ravis de vous compter parmi nous : voici les informations que vous nous avez transmises.</p>
    <br>
    <table>
        <tr>
            <td>Présent à</td>
            <td style="font-weight: bold;">{$type}</td>
        </tr>
        <tr>
            <td>Nom </td>
            <td style="font-weight: bold;">{$nom}</td>
        </tr>
        <tr>
            <td>Nombre de personnes</td>
            <td style="font-weight: bold;">{$nb_personne}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td style="font-weight: bold;">{$email}</td>
        </tr>
        <tr>
            <td>Téléphone </td>
            <td style="font-weight: bold;">{$telephone}</td>
        </tr>
    </table>

    <p style="font-weight: bold;">Si vous vous rendez compte qu'une erreur s'est glissée parmi les informations que vous nous avez fournies, contactez-nous à l'aide
        <a href="http://les-futurs.me/#contact">du formulaire de contact de notre site web</a>.</p>
</body>
</html>