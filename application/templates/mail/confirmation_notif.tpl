<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
    <h1>Confirmation de présence</h1>
    <p>{$nom} vient de confirmer sa/leur présence.</p>
    <br>
    <table>
        <tr>
            <td>Présent à</td>
            <td style="font-weight: bold;">{$type}</td>
        </tr>
        <tr>
            <td>Nom </td>
            <td style="font-weight: bold;">{$nom}</td>
        </tr>
        <tr>
            <td>Nombre de personnes</td>
            <td style="font-weight: bold;">{$nb_personne}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td style="font-weight: bold;">{$email}</td>
        </tr>
        <tr>
            <td>Téléphone </td>
            <td style="font-weight: bold;">{$telephone}</td>
        </tr>
    </table>

    <p style="font-weight: bold;">Rendez-vous sur <a href="http://les-futurs.me/backoffice?token=1dcc6c8f0b1b4a61247103b7fbfca833a72ca371">le back office pour lister toutes les confirmations</a>.</p>
</body>
</html>