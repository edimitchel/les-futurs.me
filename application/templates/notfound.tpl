{extends file="struct/template.tpl"}
{block name="title"}Page introuvable{/block}
{block name="header"}
	<button class="button alt2"><i class="fa fa-home"></i> Accueil</button>
{/block}
{block name="content"}
	<header class="major container">
		<h3>Page introuvable</h3>

		<p>Oups, vous vous êtes trompez de page ? <a href="/">Revenez à l'accueil !</a></p>
	</header>
{/block}
{block name="moreScript"}
{/block}
