{extends file="struct/template.tpl"}
{block name="title"}Back Office{/block}
{block name="header"}
    <button class="button alt2"><i class="fa fa-home"></i> Accueil</button>
{/block}
{block name="content"}
    <div class="box container">
        <section>
            <div class="table-wrapper">
                <table class="default">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Nom</th>
                        <th width="25px">Nombre</th>
                        <th>Email</th>
                        <th>Téléphone</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach $confs as $conf}
                        <tr>
                            <td>{$conf->getType()}</td>
                            <td>{$conf->getNom()}</td>
                            <td>{$conf->getNbPersonne()}</td>
                            <td>{$conf->getEmail()}</td>
                            <td>{$conf->getTelephone()}</td>
                            <td><a class="delete-conf" href="" data-id="{$conf->getId()}">&cross;</a></td>
                        </tr>
                        {foreachelse}
                        <tr>
                            <td colspan="5"><strong>Aucune confirmation recueillie</strong></td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="2"></td>
                        <td id="totalPersonne"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </section>
    </div>
{/block}
{block name="moreScript"}
    {importJs name="backoffice"}
{/block}