{extends file="struct/template.tpl"}
{block name="title"}Accueil{/block}
{block name="content"}
	<header class="major container 75%">
		<h2>Les futurs <br><strong>Monsieur et Madame EDIGHOFFER</strong></h2>
		<br>
		<br>
		<img class="image-rounded" src="{imguri name="photo-face.jpg"}" alt="Photo des futurs mariés">
		
        <p><strong>1<sup>er</sup> juillet 2017</strong><br><strong class="dayleft">{$dayleft}</strong></p>

		<p><strong>« Pose-moi sur ton cœur comme un bijou précieux,<br>
			garde-moi près de toi, comme un bracelet gravé à ton nom. »</strong><br><br>
		<i>Cantiques des cantiques 8 v.6a</i></p>

		<br>
		<hr>

		<div>
			<ul class="vertical">
				<li><a href="#confirmationPart" class="button">Confirmer votre présence</a></li>
				<br>
				<li><a href="{getRouteUrl('qui-sont-ils')}" class="button">Les futurs mariés</a></li>
				<br>
				<li><a href="{getRouteUrl('les-aider')}" class="button">Les aider pour la suite</a></li>
                <br>
                <li><a href="#contact" class="button">Contacter les mariés <span>(ou les parents)</span></a></li>
            </ul>
		</div>
	</header>

	<div class="box alt container">
		<section class="feature left">
			<a href="https://goo.gl/maps/JxJPbUusi7o" class="image"><img src="{imguri name='mairie-oberhoffen.jpg'}" alt="" /></a>
			<div class="content">
				<h3>L'union civile</h3>
				<p>Nous allons nous unir devant le maire <br> le <strong>24 JUIN 2017 À 16H</strong> à la <a href="https://goo.gl/maps/JxJPbUusi7o" target="_blank" title="Mairie">mairie d'Oberhoffen-lès-Wbg</a>.</p>
			</div>
		</section>
		<section class="feature right">
			<a href="https://goo.gl/maps/R523GSqrB3H2" class="image"><img src="{imguri name='eglise-saverne.jpg'}" alt="" /></a>
			<div class="content">
				<h3>La bénédiction du mariage</h3>
				<p>Pour témoigner de notre union, nous nous présenterons le <strong>1<sup>er</sup> JUILLET 2017</strong> À <strong>15h</strong> au <a href="https://goo.gl/maps/R523GSqrB3H2" target="_blank" title="Temple">temple protestant de Saverne</a> pour demander à Dieu de bénir notre mariage devant vous et nos témoins.</p>
			</div>
		</section>
		<section class="feature left">
			<a href="https://www.youtube.com/watch?v=5lAL1xi3gh4" class="image"><img src="{imguri name='aperitif.jpg'}" alt="" /></a>
			<div class="content">
				<h3>On fête ça juste après !</h3>
				<p>Venez profiter avec nous d'un vin d'honneur pour se réjouir ensemble de notre mariage !</p>
			</div>
		</section>
        <section class="feature right">
            <a href="https://www.youtube.com/watch?v=H6qJ_kcL1qg" class="image"><img src="{imguri name='waldolwisheim.jpg'}" alt="" /></a>
            <div class="content">
                <h3>On va manger et danser ?</h3>
                <p>Nous continuerons la fête <br>avec nos invités à Waldolwisheim.</p>
            </div>
        </section>
	</div>

	<footer class="major container 75%" id="confirmationPart">
		<h3>Dites-nous si vous serez avec nous !</h3>
		<p>Que vous soyez invité(e) à la cérémonie ou davantage, confirmer votre présence pour prévoir assez de quoi vous rassasiez.</p>
		<ul class="actions vertical">
            <li><a href="javascript:void(1);" onclick="confirmPresence('ceremonie')" class="button" title="Vous confirmez votre présence pour la cérémonie et l'apéritif.">&nbsp;Je viens uniquement à la cérémonie</a></li>
            <li><a href="javascript:void(1);" onclick="confirmPresence('soiree')" class="button alt" title="Vous confirmez votre présence à la soirée en plus de la cérémonie et l'apéritif.">Je viens à la soirée (pour les invités)</a></li>
		</ul>

		<form action="" id="confirmForm" class="box hidden">
			<h4>Présence pour <span></span></h4>
			<p><i>Nous vous contacterons peut-être quelques jours avant le mariage pour vérifier que tout est bon.</i></p>
			<div class="row">
				<div class="8u 12u(mobilep)">
					<label for="nom">Nom</label>
					<input class="text" type="text" name="nom" id="cnfnom" value="" placeholder="Seul(e) / Couple / Famille / Groupe" required="required" />
				</div>
				<div class="4u 12u(mobilep)">
					<label for="nbpersonne">Nb. de personnes</label>
					<input class="number" type="number" min="1" max="15" name="nbpersonne" id="cnfnbpersonne" value="1" placeholder="Pour mieux vous compter" required="required" />
				</div>
			</div>
			<div class="row">
				<div class="12u">
					<label for="email">Votre adresse e-mail</label>
					<input class="text" type="email" name="email" id="cnfemail" value="" placeholder="Pour vous écrire si besoin" required="required" />
				</div>
			</div>
			<div class="row">
				<div class="12u">
					<label for="phone">Votre numéro de téléphone</label>
					<input class="text" type="text" pattern="[0-9]{ldelim}10{rdelim}" name="phone" id="cnfphone" value="" placeholder="Pour vous recontacter en cas d'urgence" required="required" />
				</div>
			</div>
			<div class="row">
				<div class="12u">
					<ul class="actions">
						<li><input type="submit" value="Confirmer" /></li>
						<li><input type="reset" value="Annuler" class="alt" /></li>
					</ul>
				</div>
			</div>
		</form>
	</footer>
{/block}
{block name="moreScript"}
	{importJs name="confirm"}
{/block}
