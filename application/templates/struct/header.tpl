<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://www.les-futurs.me/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://www.les-futurs.me/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://www.les-futurs.me/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://www.les-futurs.me/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="https://www.les-futurs.me/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://www.les-futurs.me/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="https://www.les-futurs.me/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://www.les-futurs.me/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="https://www.les-futurs.me/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="https://www.les-futurs.me/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="https://www.les-futurs.me/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="https://www.les-futurs.me/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="https://www.les-futurs.me/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="Les futurs M&Mme Edighoffer - MMM"/>
        <meta name="msapplication-TileColor" content="#913131" />
        <meta name="msapplication-TileImage" content="https://www.les-futurs.me/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="https://www.les-futurs.me/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="https://www.les-futurs.me/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="https://www.les-futurs.me/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="https://www.les-futurs.me/mstile-310x310.png" />

        <!-- Twitter Card data -->
        <meta name="twitter:card" content="page">
        <meta name="twitter:title" content="Mariage de Mathilde &amp; Michel">
        <meta name="twitter:image" content="https://www.les-futurs.me/application/assets/images/photo-face.jpg">

        <!-- Open Graph data -->
        <meta property="og:title" content="Mariage de Mathilde &amp; Michel" />
        <meta property="og:type" content="page" />
        <meta property="og:url" content="https://www.les-futurs.me/" />
        <meta property="og:image" content="https://www.les-futurs.me/application/assets/images/photo-face.jpg" />
        <meta property="og:site_name" content="Les futurs monsieur &amp; madame EDIGHOFFER" />

        {if isset($page_description)}
            <meta name="description" content="{$page_description}">
            <meta name="twitter:description" content="{$page_description}">
            <meta property="og:description" content="{$page_description}" />
        {/if}

        <title>{block name="title"}{$smarty.block.child} - {$app_name}{/block}</title>

        <!--[if lte IE 8]>{importJs name="ie/html5shiv"}<![endif]-->
        {block name="moreStyle"}
        <link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet">
        {importCss name="main"}
        <!--[if lte IE 8]>{importCss name="ie8.css"}<![endif]-->
        {$smarty.block.child}
        {/block}

	<!-- Piwik -->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(["setDomains", ["*.les-futurs.me","*.www.les-futurs.me"]]);
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
	    var u="//piwik.micheledighoffer.fr/piwik/";
	    _paq.push(['setTrackerUrl', u+'piwik.php']);
	    _paq.push(['setSiteId', '2']);
	    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<noscript><p><img src="//piwik.micheledighoffer.fr/piwik/piwik.php?idsite=2" style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->
    </head>
    <body>

        <a href="/"><div id="header">
            {block name="header"}{/block}
        </div></a>
