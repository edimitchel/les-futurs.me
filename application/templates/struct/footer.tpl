
        <div id="footer">
            <div class="container 75%">
                <a name="contact"></a>
                <header class="major last">
                    <h2>Contactez-nous !</h2>
                </header>

                <p>Vous avez une question à nous poser ? À nos parents ? Aux organisateurs de la soirée ?
                    <br>N'hésitez pas à remplir le formulaire ci-dessous ou à nous écrire à l'adresse suivante :<br>
                    <strong>{mailto address="contactez@les-futurs.me" encode="javascript_charcode" subject="Confirmation / Question"}</strong></p>

                <form method="post" action="" id="formContact">
                    <div class="row">
                        <div class="12u">
                            <select name="typeMsg" title="Choissisez les destinataires" id="cnttypeMsg">
                                <option value="" disabled>Choisissez les destinataires..</option>
                                <option value="maries" selected>Pour les mariés</option>
                                <option value="organisateurs">Pour les organisateurs de la soirée</option>
                                <option value="parent_mariee">Pour les parents de la future mariée</option>
                                <option value="parent_marie">Pour les parents du futur marié</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="6u 12u(mobilep)">
                            <input type="text" name="name" placeholder="Nom / prénom" id="cntnom" required="required" />
                        </div>
                        <div class="6u 12u(mobilep)">
                            <input type="email" name="email" placeholder="Adresse e-mail" id="cntemail" required="required" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="12u">
                            <textarea name="message" placeholder="Message" rows="6" id="cntmessage" required="required"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="12u">
                            <ul class="actions">
                                <li><input type="submit" value="Envoyer le message" /></li>
                            </ul>
                        </div>
                    </div>
                </form>

                <ul class="copyright">
                    <li>2017 – Mariage de Mathilde et Michel</li>
                    <li><a href="/">Accueil</a> – <a href="/qui-sont-ils">Qui sont-ils ?</a> – <a href="/les-aider">Les soutenir</a></li>
                </ul>
            </div>
        </div>
		{block name="moreScript"}
        {importJs name="jquery.min"}
        {importJs name="skel.min"}
        {importJs name="util"}
        <!--[if lte IE 8]>{importJs name="ie/respond.min"}<![endif]-->
        {importJs name="main"}
        {importJs name="contact"}
        {$smarty.block.child}
		{/block}
	</body>
</html>