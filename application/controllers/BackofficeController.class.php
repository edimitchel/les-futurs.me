<?php


class BackofficeController extends BaseController
{
    protected function before()
    {
        $auth = $_GET['token'] ?? false;

        if($auth !== '1dcc6c8f0b1b4a61247103b7fbfca833a72ca371') {
            if(!$this->request->isAjaxRequest())
                FrontController::redirect('/notfound');
            $this->getResponse()->setError('message', 'Non autorisé');
            die();
        }
    }

    public function index()
    {
        $c = Confirmation::getAll();

        $this->attach('confs', $c);
    }

    public function a_deleteconf($id) {
        $r = Confirmation::delete($id);
        if($r) {
            $this->getResponse()->setData('message', 'Suppression réussie');
        } else {
            $this->getResponse()->setError('message', 'Suppression échouée');
        }
    }
}