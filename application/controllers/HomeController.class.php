<?php


class HomeController extends BaseController
{

    public function index()
    {
        $dateM = new DateTime();
        $dateM->setDate(2017, 7, 1);
        $dateM->setTime(15, 0, 0);
        $now = new DateTime('now');
        $diff = $dateM->diff($now);
        $d = $diff->days;
        if($d == 0 && $diff->invert == 1) {
            if($diff->h > 0)
                $this->attach('dayleft', 'Cérémonie dans ' . $diff->h . ' heure' . ($diff->h > 1 ? 's' : ''));
            else if($diff->i > 0)
                $this->attach('dayleft', 'Cérémonie dans ' . $diff->i . ' minute' . ($diff->i > 1 ? 's' : ''));
        } else {
            if($diff->h <= 1 && $diff->invert == 0) {
                $this->attach('dayleft', "VENEZ, ça a commencé !");
            } else {
                $this->attach('dayleft', $d > 0 ? 'J-' . $d : 'Mariés depuis ' . ($d === 0 ? 'AUJOURD\'HUI' : -$d . ' jours !!'));
            }
        }
        $this->attach('page_description', "Nous nous présentons à vous pour vous invitez à vivre avec nous notre mariage le 1er juillet 2017 à l'église protestante de Saverne à 15h.");
    }

    private $destinataires = [
        "maries" => array(
            "edimitchel@gmail.com",
            "mathildecarel@gmail.com"
        ),
        "organisateurs" => array(
            "animation.mmm@gmail.com"
        ),
        "parent_mariee" => array(
            "jlcarel@orange.fr",
            "magalicarel67@gmail.com"
        ),
        "parent_marie" => array(
            "aidizen@yahoo.fr",
            "sylvie.edighoffer@gmail.com"
        ),
    ];

    private $contactMail = 'contactez@les-futurs.me';

    public function a_contact($destinataire = null)
    {
        if (null === $destinataire || !isset($this->destinataires[$destinataire])) {
            $this->getResponse()->setError('destinataire', "Destinataire introuvable");
            return;
        }

        $nom = Request::post('nom');
        $adresse = Request::post('adresse', Request::EMAIL);
        $message = nl2br(strip_tags(Request::post('message')));

        if (isset($nom, $adresse, $message) && $adresse !== false) {

            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->addReplyTo($adresse, $nom);
            $mail->setFrom($adresse, $nom);
            foreach ($this->destinataires[$destinataire] as $dest) {
                $mail->addAddress($dest);
            }
            $mail->isHTML(true);

            if (Configuration::get('env') == 'development') {
                $mail->isSMTP();
                $mail->Username = Configuration::get('mail_user') . '@gmail.com';
                $mail->Password = Configuration::get('mail_password');
                $mail->Host = "smtp.gmail.com";
                $mail->Port = 465;
                $mail->SMTPSecure = 'ssl';
                $mail->SMTPAuth = true;
            }
            $template = AACSmarty::getInstance(true);
            $template->assign('message', $message);

            $mail->Subject = "les-futurs.me : un nouveau message pour vous";

            $mail->msgHTML($template->fetch('mail/message.tpl'));
            $mail->AltBody = strip_tags($mail->Body);

            if ($mail->send()) {
                $this->getResponse()->setData('message', 'Le message a bien été envoyé');
            } else {
                $this->getResponse()->setError('mail', "Une erreur est survenue lors de l'envoi du message");
            }
        } else {
            $this->getResponse()->setError('data', "Les données ne sont pas correctes");
        }
    }

    public function a_confirm()
    {
        $type = Request::post('type');
        $nom = Request::post('nom');
        $nbPersonne = Request::post('nbpersonne', Request::INT);
        $adresse = Request::post('adresse', Request::EMAIL);
        $phone = Request::post('phone');

        if (isset($nom, $nbPersonne, $adresse, $phone) && $nbPersonne !== false && $adresse !== false) {

            $typeConf = $type == 'soiree' ? 'à la célébration' : 'à la cérémonie & apéritif';

            $c = new Confirmation;
            $c->setType($type)->setNom($nom)->setNbPersonne($nbPersonne)->setEmail($adresse)->setTelephone($phone);

            $c->create();

            // ENVOI MAIL AUX MARIES

            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->addReplyTo($adresse, $nom);
            $mail->setFrom($adresse, $nom);
            $mail->addAddress($this->contactMail);
            $mail->isHTML(true);

            if (Configuration::get('env') == 'development') {
                $mail->isSMTP();
                $mail->Username = Configuration::get('mail_user') . '@gmail.com';
                $mail->Password = Configuration::get('mail_password');
                $mail->Host = "smtp.gmail.com";
                $mail->Port = 465;
                $mail->SMTPSecure = 'ssl';
                $mail->SMTPAuth = true;
            }
            $template = AACSmarty::getInstance(true);
            $template->assign('type', $c->getType(true));
            $template->assign('nom', $c->getNom());
            $template->assign('nb_personne', $c->getNbPersonne());
            $template->assign('email', $c->getEmail());
            $template->assign('telephone', $c->getTelephone());

            $mail->Subject = "les-futurs.me : nouvelle confirmation de présence de ". $c->getNom();

            $mail->msgHTML($template->fetch('mail/confirmation_notif.tpl'));
            $mail->AltBody = strip_tags($mail->Body);

            if (!$mail->send()) {
                $this->getResponse()->setError('mail', "Une erreur est survenue lors de l'envoi de la notification de l'ajout.");
            }

            // ENVOI MAIL AUX PERSONNES QUI CONFIRME

            $mail = new PHPMailer();
            $mail->CharSet = 'UTF-8';
            $mail->addReplyTo("contactez@les-futurs.me", "Les futurs mariés");
            $mail->setFrom("contactez@les-futurs.me", "Les futurs mariés: Michel et Mathilde");
            $mail->addAddress($adresse);

            $mail->isHTML(true);

            if (Configuration::get('env') == 'development') {
                $mail->isSMTP();
                $mail->Username = Configuration::get('mail_user') . '@gmail.com';
                $mail->Password = Configuration::get('mail_password');
                $mail->Host = "smtp.gmail.com";
                $mail->Port = 465;
                $mail->SMTPSecure = 'ssl';
                $mail->SMTPAuth = true;
            }
            $template = AACSmarty::getInstance(true);
            $template->assign('type', $c->getType(true));
            $template->assign('nom', $c->getNom());
            $template->assign('nb_personne', $c->getNbPersonne());
            $template->assign('email', $c->getEmail());
            $template->assign('telephone', $c->getTelephone());

            $mail->Subject = "les-futurs.me : votre confirmation de présence à notre mariage";

            $mail->msgHTML($template->fetch('mail/confirmation_user.tpl'));
            $mail->AltBody = strip_tags($mail->Body);

            $mail->send();

            $this->getResponse()->setData('message', 'Votre confirmation de ' . $nbPersonne . ' personne' . ($nbPersonne > 1 ? 's' : '') . ' ' . $typeConf . ' a été enregistrée. <br>Un mail de confirmation vous a été envoyé.');

        } else {
            $this->getResponse()->setError('data', "Les données ne sont pas correctes");
        }
    }
}