<?php


class QuisontilsController extends BaseController
{

    public function index()
    {
        $this->attach('page_description', "Une petite présentation des futurs mariés ne vous fera pas de mal.");

        $ageMichel = (new DateTime('now'))->diff((new DateTime())->setDate(1993, 02, 28)->setTime(0, 40, 0))->format('%Y');
        $ageMathilde = (new DateTime('now'))->diff((new DateTime())->setDate(1992, 05, 29)->setTime(14, 00, 0))->format('%Y');

        $this->attach('ageMichel', $ageMichel);
        $this->attach('ageMathilde', $ageMathilde);
    }
}