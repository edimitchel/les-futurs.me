<?php

define('APP_DIR', 'application');

define('ASSETS_DIR', 'assets');

define('SYSTEM_DIR', 'system');


define('APP_PATH', __DIR__ . '/' . APP_DIR . '/');

define('SYSTEM_PATH', __DIR__ . '/' . SYSTEM_DIR . '/');

define('VENDOR_PATH', APP_DIR . '/' . 'vendor' . '/');

define('ASSETS_PATH', APP_DIR . '/' . ASSETS_DIR . '/');

define('LAZER_DATA_PATH', SYSTEM_PATH . 'database/lazer/');

if (!defined('NO_INIT')) {
    require_once SYSTEM_PATH . 'init.php';

    Configuration::getConfigFromFile(SYSTEM_PATH . 'config.conf');
    Configuration::getConfigFromFile(SYSTEM_PATH . 'database.conf');

    // Définition de la page NotFound
    FrontController::setNotFoundPage('notfound');

    // Ajout des tâches à exécuter

    // Affiche le contrôleur suivant les paramètres dans le requête
    FrontController::dispatch();
}
