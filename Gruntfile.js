const grunt = require('grunt');
require('load-grunt-tasks')(grunt);

grunt.initConfig({
    sass: {
        options: {
            sourceMap: true
        },
        dist: {
            files: {
                'application/assets/css/main.css': 'application/assets/scss/main.scss'
            }
        }
    },
    watch: {
        scripts: {
            files: ['**/*.scss'],
            tasks: ['sass'],
            options: {
                spawn: false
            }
        }
    }
});

grunt.registerTask('default', ['sass', 'watch']);